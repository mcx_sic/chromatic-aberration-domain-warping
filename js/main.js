var scene = new THREE.Scene();
var renderer = new THREE.WebGLRenderer();
document.body.appendChild(renderer.domElement);

// Render target
var camera = new THREE.OrthographicCamera(
    window.innerWidth / -2, window.innerWidth / 2, 
    window.innerHeight / 2, window.innerHeight / -2, 
    0.01, 1000);
camera.position.z = 10;

var noiseShader = document.getElementById('noiseShader').textContent;

var texture = new THREE.TextureLoader().load( 'textures/mish.png' );
var ar = texture.image.width / texture.image.height;

// Main render
var geometry = new THREE.PlaneBufferGeometry(window.innerWidth, window.innerHeight);
var uniforms = {
    u_time: { value: 1.0 },
    u_general_scale: { value: 1.0 },
    u_texture_ar: { value: 1920/1080 },
    u_noise_scale: {
        value: new THREE.Vector2(1.0, 1.0)
    },
    u_noise_position: {
        value: new THREE.Vector2(0.0, 0.0)
    },
    u_color1: {
        value: new THREE.Color('rgb(232, 232, 230)')
    },
    u_color2: {
        value: new THREE.Color('rgb(209, 188, 102)')
    },
    u_texture: { type: "t", value: texture }
};

var material = new THREE.ShaderMaterial({
    map: texture,
    uniforms: uniforms,
    vertexShader: document.getElementById('vertexShader').textContent,
    fragmentShader: document.getElementById('fragmentShader').textContent.replace("// {=SHADER_METHODS} //", noiseShader)
});

var mesh = new THREE.Mesh(geometry, material);
scene.add(mesh);

window.addEventListener('resize', onResize);
onResize();

function onResize() {
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setPixelRatio(0.7);

    camera.left = window.innerWidth / -2;
    camera.right = window.innerWidth / 2;
    camera.top = window.innerHeight / 2;
    camera.bottom = window.innerHeight / -2;
    camera.updateProjectionMatrix();

    geometry = new THREE.PlaneBufferGeometry(window.innerWidth, window.innerHeight);
    mesh.geometry.dispose();
    mesh.geometry = geometry;

    // uniforms.u_resolution.value.set(
    //     window.devicePixelRatio * window.innerWidth,
    //     window.devicePixelRatio * window.innerHeight);

    var ar = window.innerWidth / window.innerHeight;
    uniforms.u_noise_scale.value.set(1.0, 1.0);
    uniforms.u_noise_position.value.set(0.0, 0.0);

    if (ar > 1) {
        uniforms.u_noise_scale.value.y = 1 / ar;
        uniforms.u_noise_position.value.y = 1. - uniforms.u_noise_scale.value.y;
    } else {
        uniforms.u_noise_scale.value.x = ar;
        uniforms.u_noise_position.value.x = 1. - uniforms.u_noise_scale.value.x;
    }

}

function animate(timestamp) {
    requestAnimationFrame(animate);
    uniforms.u_time.value = timestamp / 5000;
    renderer.render(scene, camera);
}

animate();